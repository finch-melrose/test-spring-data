package finch.bug;

import finch.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface BugRepository extends JpaRepository<Bug, Long> {

    Bug findByUser(User user);

    Bug findByUserId(long userId);

    @Query("select b from Bug b where b.user.id = :userId")
    Bug findByQuery(long userId);

}
