package finch;

import finch.bug.BugRepository;
import finch.user.User;
import finch.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class TestApplication {

    @Autowired
    private BugRepository bugRepository;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test() {
        System.out.println("create user");
        User user = userRepository.save(new User());

        System.out.println("1 query: findByUser");
        bugRepository.findByUser(user);

        System.out.println("2 query: findByUserId");
        bugRepository.findByUserId(1L);

        System.out.println("3 query: findByQuery");
        bugRepository.findByQuery(1L);

        return "ok";
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TestApplication.class, args);
    }


}
